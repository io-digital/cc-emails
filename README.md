## Century City Newsletters

This repo contains CC email newsletters. 

The purpose of this repository is to store newsletters and templates. [mjml](https://mjml.io/), a responsive email framework is used so we can focus on the content instead of the email client incompatibilities.